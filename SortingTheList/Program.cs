﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SortingTheList
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Workers> ListWorkers = new List<Workers>();
            for (; ; )
            {
                Console.WriteLine("1 - Добавить данные\n2 - Сортировка\n3 - Выход");
                switch (Console.ReadLine())
                {
                    case "1":
                        ListWorkers = InputData();
                        ShowTheList(ListWorkers);
                        break;
                    case "2": 
                        SortTheList(ListWorkers);
                        ShowTheList(ListWorkers);
                        break;
                    case "3": return;
                }
            }
        }

        static List<Workers> InputData()
        {
            List<Workers> ListWorkers = new List<Workers>();
            do
            {
                Workers person = new Workers();
                Console.WriteLine("Введите имя");
                person.AddFirstName(Console.ReadLine());
                Console.WriteLine("Введите фамилию");
                person.AddSecondName(Console.ReadLine());
                Console.WriteLine("Введите возраст");
                person.AddAge(Console.ReadLine());
                ListWorkers.Add(person);
            } while (PollForInput());
            return ListWorkers;
        }

        static void SortTheList(List<Workers> ListWorkers)
        {
            Console.WriteLine("Выберите тип сортировки: 1 - по имени, 2 -  по фамилии, 3 - по возрасту.");
            //List<Workers> SortList = new List<Workers>();
            switch (Console.ReadLine())
            {
                case "1":
                    ListWorkers.Sort(delegate(Workers w1, Workers w2)
                    { return w1._FirstName.CompareTo(w2._FirstName); });
                    break;
                case "2":
                    ListWorkers.Sort(delegate(Workers w1, Workers w2)
                    { return w1._SecondName.CompareTo(w2._SecondName); });
                    break;
                case "3": 
                    ListWorkers.Sort(delegate(Workers w1, Workers w2)
                    { return w1._Age.CompareTo(w2._Age); });
                    break;
            }
        }

        static bool PollForInput()
        {
            Console.WriteLine("Хотите еще добавить данные?");
            if (Console.ReadLine().ToUpper() == "Y")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        static void ShowTheList(List<Workers> ListWorkers)
        {
            Console.WriteLine("\nИмя\tФамилия\tВозраст");
            foreach (Workers x in ListWorkers)
            {
                x.ShowWorker();
            }
        }
    }
    class Workers
    {
        public string _FirstName, _SecondName, _Age;

        public void AddFirstName(string FirstName)
        {
            _FirstName = FirstName;
        }

        public void AddSecondName(string SecondName)
        {
            _SecondName = SecondName;
        }

        public void AddAge(string Age)
        {
            _Age = Age;
        }

        public void ShowWorker()
        {
            Console.WriteLine("{0}\t{1}\t{2}", _FirstName, _SecondName, _Age);
        }
    }
}
